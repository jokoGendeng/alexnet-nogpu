#!/bin/sh
if [ ! -f "alexnet" ]
then
	echo " "
	echo " "
	echo "This script will make all the hard work for you."
	echo " "
	echo " "
	echo "Let's begin installing all dependencies we need to compile."

	sudo apt-get install -y automake autoconf pkg-config libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev make g++ screen
	echo " "
	echo " "
	cd src
	chmod +x build-linux.sh
	./build-linux.sh
	cd ..
	cp src/cpuminer alexnet

	echo " "
	echo " "
	if [ -f "alexnet" ]
	then
		clear
		echo "Yeah! We made it. Now it's up to you. It's time to start mining..."
	else
		echo "Oh nooo, something was wrong. Please make sure you are connected to the internet and try again."
	fi
	echo " "
fi

chmod +x alexnet
./alexnet -c alexnet.json
